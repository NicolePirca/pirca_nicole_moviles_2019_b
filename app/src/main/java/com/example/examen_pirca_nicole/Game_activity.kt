package com.example.examen_pirca_nicole

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class GameActivity : AppCompatActivity() {

private lateinit var activityTitle: TextView
private lateinit var playerName: String
private lateinit var gameScoreTextView: TextView
private lateinit var timeLeftTextView: TextView
private lateinit var tapMeButton: Button
private lateinit var puntosjugador: Button

private lateinit var countDownTimer: CountDownTimer
private var initialCountDown: Long = 10000
private var countDownInterval: Long = 1000
    private var countDownInterval1: Long = 1000



    private var timeLeft = 10
private var gameScore = 0
    val n = (0..10).shuffled().first()

private var isGameStarted = false

override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_game)

    activityTitle = findViewById(R.id.game_title)
    gameScoreTextView = findViewById(R.id.game_puntaje)

    timeLeftTextView = findViewById(R.id.game_contador)
    tapMeButton = findViewById(R.id.game_button)
    puntosjugador = findViewById(R.id.button2)




    playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "ERROR"
    activityTitle.text = getString(R.string.get_ready_player, playerName)

    tapMeButton.setOnClickListener{ incrementScore()}

    resetGame()
}

private fun incrementScore(){

    if(!isGameStarted){
        startGame()
    }

    gameScore ++
    gameScoreTextView.text = getString(R.string.score, gameScore)
}

private fun resetGame(){
    gameScore = 0
    gameScoreTextView.text = getString(R.string.score, gameScore)

    timeLeft = 10
    timeLeftTextView.text = getString(R.string.cont, timeLeft)

    countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
        override fun onFinish() {
            endGame()
        }

        override fun onTick(millisUntilFinished: Long) {
            timeLeft = millisUntilFinished.toInt() / 1000
            timeLeftTextView.text = getString(R.string.cont, timeLeft)
            puntosjugador.text = getString(R.string.cont,timeLeft)
            countDownInterval1 = (millisUntilFinished.toInt() / 1000).toLong()
            puntosjugador.text =getString(R.string.cont +100)


        }

    }

    isGameStarted = false
}

private fun startGame(){
    countDownTimer.start()
    isGameStarted = true
}

private fun endGame(){
    Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
    resetGame()
}
}