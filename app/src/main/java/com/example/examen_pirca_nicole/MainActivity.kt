package com.example.examen_pirca_nicole

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var startButton: Button
    private lateinit var playerName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton = findViewById(R.id.start_button)
        playerName = findViewById(R.id.player_name_input)

        startButton.setOnClickListener { showGameActivity() }

    }

    fun showGameActivity(){

        val gameActivityIntent = Intent(this, GameActivity::class.java)

        gameActivityIntent.putExtra(INTENT_PLAYER_NAME, playerName.text.toString())

        startActivity(gameActivityIntent)

    }

    companion object {
        const val INTENT_PLAYER_NAME = "playerName"
    }
}
